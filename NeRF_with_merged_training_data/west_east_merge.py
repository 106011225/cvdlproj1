import json
import os
import shutil
import numpy as np
import math

processingDataset = 'west_east_merge'

# input json
f_lego_train = open('./lego_copy/transforms_train.json', mode='r')
f_lego_val = open('./lego_copy/transforms_val.json', mode='r')
f_lego_test = open('./lego_copy/transforms_test.json', mode='r')
data_lego_train = json.load(f_lego_train)
data_lego_val = json.load(f_lego_val)
data_lego_test = json.load(f_lego_test)
f_lego_train.close()
f_lego_val.close()
f_lego_test.close()
f_chair_train = open('./chair_copy/transforms_train.json', mode='r')
f_chair_val = open('./chair_copy/transforms_val.json', mode='r')
f_chair_test = open('./chair_copy/transforms_test.json', mode='r')
data_chair_train = json.load(f_chair_train)
data_chair_val = json.load(f_chair_val)
data_chair_test = json.load(f_chair_test)
f_chair_train.close()
f_chair_val.close()
f_chair_test.close()
    # keys: ['camera_angle_x', 'frames'] 
    #     camera_angle_x: float
    #     frames: list of dict of path & rt (len = 200 for test & 100 for others)


# mkdir
if not os.path.exists(f'./{processingDataset}/train/'):
    os.mkdir(f'./{processingDataset}/train/')
if not os.path.exists(f'./{processingDataset}/val/'):
    os.mkdir(f'./{processingDataset}/val/')
if not os.path.exists(f'./{processingDataset}/test/'):
    os.mkdir(f'./{processingDataset}/test/')



# Checks if a matrix is a valid rotation matrix.
def isRotationMatrix(R) :
    Rt = np.transpose(R)
    shouldBeIdentity = np.dot(Rt, R)
    I = np.identity(3, dtype = R.dtype)
    n = np.linalg.norm(I - shouldBeIdentity)
    return n < 1e-5
 
# Calculates rotation matrix to euler angles (in radian, between Pi to -Pi)
def rotationMatrixToEulerAngles(R) :
    assert(isRotationMatrix(R))
    sy = math.sqrt(R[0,0] * R[0,0] +  R[1,0] * R[1,0])
    singular = sy < 1e-6

    if  not singular :
        x = math.atan2(R[2,1] , R[2,2])
        y = math.atan2(-R[2,0], sy)
        z = math.atan2(R[1,0], R[0,0])
    else :
        x = math.atan2(-R[1,2], R[1,1])
        y = math.atan2(-R[2,0], sy)
        z = 0
 
    return np.array([x, y, z])


# Angles:
#   X always > 0
#   Y often > 0 (about the same for chair)
#   Z about the same freq between >0 & <0


def copyIMG(imgName, imgType, imgNum_old, imgNum_new):
    '''
    imgName: lego or chair
    imgType: train or val or test
    imgNum: 0~99 or 0~199
    '''
    if imgType == 'test':
        shutil.copyfile(f'./{imgName}_copy/{imgType}/r_{imgNum_old}.png',
                        f'./{processingDataset}/{imgType}/r_{imgNum_new}.png')
        if imgName == 'lego':
            shutil.copyfile(f'./{imgName}_copy/{imgType}/r_{imgNum_old}_depth_0001.png',
                            f'./{processingDataset}/{imgType}/r_{imgNum_new}_depth_0000.png')
            shutil.copyfile(f'./{imgName}_copy/{imgType}/r_{imgNum_old}_normal_0001.png',
                            f'./{processingDataset}/{imgType}/r_{imgNum_new}_normal_0000.png')
        else:
            shutil.copyfile(f'./{imgName}_copy/{imgType}/r_{imgNum_old}_depth_0000.png',
                            f'./{processingDataset}/{imgType}/r_{imgNum_new}_depth_0000.png')
            shutil.copyfile(f'./{imgName}_copy/{imgType}/r_{imgNum_old}_normal_0000.png',
                            f'./{processingDataset}/{imgType}/r_{imgNum_new}_normal_0000.png')
    else:
        shutil.copyfile(f'./{imgName}_copy/{imgType}/r_{imgNum_old}.png',
                        f'./{processingDataset}/{imgType}/r_{imgNum_new}.png')



# processing
# train
newImgIdCounter = 0
data_merge_train = {}
data_merge_train['camera_angle_x'] = data_lego_train['camera_angle_x']
data_merge_train['frames'] = []
for i in range(len(data_lego_train['frames'])):

    img_L = data_lego_train['frames'][i]
    rotMat_L = np.array(img_L['transform_matrix'])[0:3, 0:3]
    Angles_L = rotationMatrixToEulerAngles(rotMat_L)
    if Angles_L[2]  >= 0:
        img_L['file_path'] = f'./train/r_{newImgIdCounter}'
        data_merge_train['frames'].append(img_L)
        copyIMG('lego', 'train', i, newImgIdCounter)
        newImgIdCounter += 1

    img_C = data_chair_train['frames'][i]
    rotMat_C = np.array(img_C['transform_matrix'])[0:3, 0:3]
    Angles_C = rotationMatrixToEulerAngles(rotMat_C)
    if Angles_C[2] < 0:
        img_C['file_path'] = f'./train/r_{newImgIdCounter}'
        data_merge_train['frames'].append(img_C)
        copyIMG('chair', 'train', i, newImgIdCounter)
        newImgIdCounter += 1

#val
newImgIdCounter = 0
data_merge_val = {}
data_merge_val['camera_angle_x'] = data_lego_val['camera_angle_x']
data_merge_val['frames'] = []
for i in range(len(data_lego_val['frames'])):

    img_L = data_lego_val['frames'][i]
    rotMat_L = np.array(img_L['transform_matrix'])[0:3, 0:3]
    Angles_L = rotationMatrixToEulerAngles(rotMat_L)
    if Angles_L[2]  >= 0:
        img_L['file_path'] = f'./val/r_{newImgIdCounter}'
        data_merge_val['frames'].append(img_L)
        copyIMG('lego', 'val', i, newImgIdCounter)
        newImgIdCounter += 1

    img_C = data_chair_val['frames'][i]
    rotMat_C = np.array(img_C['transform_matrix'])[0:3, 0:3]
    Angles_C = rotationMatrixToEulerAngles(rotMat_C)
    if Angles_C[2] < 0:
        img_C['file_path'] = f'./val/r_{newImgIdCounter}'
        data_merge_val['frames'].append(img_C)
        copyIMG('chair', 'val', i, newImgIdCounter)
        newImgIdCounter += 1

#test
newImgIdCounter = 0
data_merge_test = {}
data_merge_test['camera_angle_x'] = data_lego_test['camera_angle_x']
data_merge_test['frames'] = []
for i in range(len(data_lego_test['frames'])):

    img_L = data_lego_test['frames'][i]
    rotMat_L = np.array(img_L['transform_matrix'])[0:3, 0:3]
    Angles_L = rotationMatrixToEulerAngles(rotMat_L)
    if Angles_L[2]  >= 0:
        img_L['file_path'] = f'./test/r_{newImgIdCounter}'
        data_merge_test['frames'].append(img_L)
        copyIMG('lego', 'test', i, newImgIdCounter)
        newImgIdCounter += 1

    img_C = data_chair_test['frames'][i]
    rotMat_C = np.array(img_C['transform_matrix'])[0:3, 0:3]
    Angles_C = rotationMatrixToEulerAngles(rotMat_C)
    if Angles_C[2] < 0:
        img_C['file_path'] = f'./test/r_{newImgIdCounter}'
        data_merge_test['frames'].append(img_C)
        copyIMG('chair', 'test', i, newImgIdCounter)
        newImgIdCounter += 1




# save json
with open(f'./{processingDataset}/transforms_train.json', mode='w', encoding='utf-8') as f:
    json.dump(data_merge_train, f, ensure_ascii=False, indent=4)
with open(f'./{processingDataset}/transforms_val.json', mode='w', encoding='utf-8') as f:
    json.dump(data_merge_val, f, ensure_ascii=False, indent=4)
with open(f'./{processingDataset}/transforms_test.json', mode='w', encoding='utf-8') as f:
    json.dump(data_merge_test, f, ensure_ascii=False, indent=4)



