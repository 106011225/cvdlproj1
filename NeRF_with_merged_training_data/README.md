# [Computer Vision] NeRF with merged training data


## NeRF: Representing Scenes as Neural Radiance Fields for View Synthesis

Original paper:  
[Mildenhall, B., Srinivasan, P., Tancik, M., Barron, J., Ramamoorthi, R., & Ng, R.. (2020). NeRF: Representing Scenes as Neural Radiance Fields for View Synthesis.](https://arxiv.org/abs/2003.08934)


> A neural radiance field is a simple fully connected network (weights are ~5MB) trained to reproduce input views of a single scene using a rendering loss. The network directly maps from spatial location and viewing direction (5D input) to color and opacity (4D output), acting as the "volume" so we can use volume rendering to differentiably render new views.

(cited from https://github.com/bmild/nerf)


## Goal

- Modify the original training dataset and observe the difference between the results 
- For example, select all **West** side **lego** training data images & all **East** side **chair** training data images
    - Merge the two halves and view these images as a single dataset
    - Please refer to [west_east_merge.py](./west_east_merge.py) for implementation details

#### Original results of NeRF paper

<img src="./assets/lego.gif"> <img src="./assets/chair.gif">  

#### After merging training dataset

|West(W) side lege &<br> East(E) side chair| NW, SE side lego &<br> SW, NE side chair | Alternating between<br>lego & chair|
|:---:|:---:|:---:|
| <img src="./assets/west_east_merge.gif"> | <img src="./assets/alternating_merge.gif"> | <img src="./assets/odd_even_merge.gif"> |



## Dataset

The dataset used in this repo can be found at the [NeRF Project Page](https://www.matthewtancik.com/nerf) 


## Usage

The NeRF implementation source code is forked from   
Quei-An, C. (2020). Nerf_pl: a pytorch-lightning implementation of NeRF. https://github.com/kwea123/nerf_pl/

Please refer to [Main.ipynb](./Main.ipynb) for more details.


