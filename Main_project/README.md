# [Computer Vision] An approach to representing images as grid-based implicit functions with controllable level of details


## Introduction

- Recently, **neural rendering** is a rapidly emerging field that aims to combine the advantages of computer graphics (photo-realistic rendering) and generative machine learning. 
    - There are lots of applications related to it. For instance, telepresence with face/body reenactment, synthesizing novel views from 2D images, and scene relighting.
- To address these problems, it is common that we map the information in the image domain to another high-dimensional scene representation, then map it back to the original domain. As a result, it is critical to improve **scene representations**. 
- Lines of research represent scenes, images, 3D objects, etc, as a **grid-based model** or an **implicit function**. 
    - The former, such as voxel grids, may have **less inference time** than the latter because of the tree-like structure. However, it **costs lots of memory**, which may have a space complexity of $`O(n^3)`$. 
    - The latter can be much **more compact** than the former, but it often **takes more time** to infer the output owing to a large MLP. 
- Therefore, I propose an approach that **combines the advantages of these two methods**. 
    - To verify the performance, let the model memorize an image, and I compare the PSNR of reconstructed images with the ground truth of different methods. 
    - Also, I try to apply some transformation to this representation directly.


## Dataset

The dataset used in this repo can be found at https://www.kaggle.com/datasets/andrewmvd/animal-faces and 
https://live.staticflickr.com/7492/15677707699_d9d67acf9d_b.jpg


## Implementation 

Network diagram:  
<img src="./assets/network_diagram.png" width=600>

- I represent a *feature* on the image (or precisely, $`d(feature)`$) by a high-dimensional vector with L2 norm = 1. 
    - That is, a vector that points from the origin to somewhere on the unit sphere in a high dimensional space. 
- By placing these vectors on the image in different directions, the information in the image can be described.
- To get the RGB values of a block region on the image, we first find the 4 vectors at the corners of the block and interpolate these 4 vectors to get the *features* of all pixels in this block. By passing these vectors through a small MLP, we get the RGB values.


## Results

- Testing on the animal face dataset with image size 512x512, this model can learn a high-frequency function to map (x,y) coordinate to RGB value, and the details such as animal whiskers can be reconstructed. 
- In comparison with *Gaussian position encoding (Tancik et al. 2020)*, which only uses MLP to memorize an image, this method **saves about 32% of inference time** to achieve about the same PSNR. 
- With this model, we can take advantage of the grid-based method, while preserving the details of an image.

| Main results | Ground truth |
|:---:|:---:|
| <img src="./assets/standard.gif" width="300"> | <img src="./assets/GT.jpg" width="300"> |


## Usage

The source code can be found in [code.ipynb](./code.ipynb).  

Please refer to [poster.pdf](./poster.pdf), [slides.pdf](./slides.pdf) and [report.pdf](./report.pdf) for more details.


